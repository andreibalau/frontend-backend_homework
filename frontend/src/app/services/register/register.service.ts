import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  static readonly API_URL = 'http://localhost:8080';

  constructor(private httpClient: HttpClient) { }

  register(username: string, email: string, password: string, secretQ: string, secretA: string) {
    return this.httpClient.post(`${RegisterService.API_URL}/register`, {
      username,
      email,
      password,
      secretQ,
      secretA
    }).toPromise()
      .then((response: any) => {
        if (response) {
          return true;
        } else {
          return false;
        }
      })
      .catch((err) => {
        return false;
      });
  }

}
