import { Component, OnInit } from '@angular/core';
import List from 'src/app/models/List';
import { TodoService } from 'src/app/services/todo/todo.service';

@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  lists: List[];
  selectedList: List;

  constructor(private todoService: TodoService) { }

  async ngOnInit() {
    this.lists = await this.todoService.getLists();
  }

  onClick(list: List) {
    this.selectedList = list;
  }

}
