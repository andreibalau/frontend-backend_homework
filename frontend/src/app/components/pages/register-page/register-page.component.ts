import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from 'src/app/services/register/register.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  username: string;
  email: string;
  retypeemail: string;
  password: string;
  retypepass: string
  secretQ: string;
  secretA: string;

  successShown = false;
  errorShown = false;

  validUser = false;// unimplemented , on this one i have a bit more to work cause i need to check DB if username exists
  validPass = false;//if it doesn't have strange chars and so on is true
  confPass = false;//validate retype pass , if both password and retypepass equals then true
  //i dont check if email exists id DB ,i must do the same as i want at validUser 
  validMail = false;//if it does respects a mail format and doesn't have strange chars is true
  confMails = false;//validates retype email , if both email and retypeemail equals then true

  constructor(
    private registerService: RegisterService,
    private router: Router) { }

  ngOnInit() {
  }
  async backToLogin() {//todo this one is for when i click back to go at main page 
    await this.router.navigate(["/login"]);
  }

  filterMail() {
    let format = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // return format.test(String(this.email).toLowerCase());
    if (!format.test(String(this.email).toLowerCase() && this.email)) {
      console.log('error special chars detected to email');
      this.validMail = false;
    } else if (!this.email) {
      console.log('null value for email');
      this.validMail = false;
    } else {
      this.validMail = true;
      console.log(`${this.email} ,valid = ${this.validMail}`);
    }
  }

  checkBothMails() {
    if (this.email == null && this.retypeemail == null) {
      this.confMails = false;
      console.log(`null email values ${this.email} ${this.retypeemail} `);
    }
    else if (this.email == this.retypeemail) {
      this.confMails = true;
      console.log(`${this.email} == ${this.retypeemail}`);
      console.log(`does emails matches? :${this.confMails}`);
    } else {
      this.confMails = false;
      console.log(`${this.email} == ${this.retypeemail}`);
      console.log(`does emails matches? :${this.confMails}`);
    }
  }

  filterPass() {
    let format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;
    if (format.test(this.password) && this.password) {
      console.log('error special chars detected to pass');
      this.validPass = false;
    } else if (!this.password) {
      console.log('null value for password');
      this.validPass = false;
      console.log(`null value for password ,valid = ${this.validPass}`);
    } else {
      this.validPass = true;
      console.log(`${this.password} ,valid = ${this.validPass}`);
    }
  }

  checkBothPass() {
    if (this.password == null && this.retypepass == null) {
      this.confPass = false;
      console.log(`null passwords values`);
    }
    else if (this.password == this.retypepass) {
      this.confPass = true;
      console.log(`${this.password} == ${this.retypepass}`);
      console.log(`does pass matches? :${this.confPass}`);
    } else {
      this.confPass = false;
      console.log(`${this.password} == ${this.retypepass}`);
      console.log(`does pass matches? :${this.confPass}`);
    }
  }

  async onClickRegister() {
    console.log(this.username,
      this.email,
      this.password,
      this.secretQ,
      this.secretA);
    let registerWasSuccessful = await this.registerService.register(this.username,
      this.email,
      this.password,
      this.secretQ,
      this.secretA);
    if (registerWasSuccessful) {
      window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
      this.successShown = true;
      setTimeout(() => {
        this.router.navigate(["/login"]);
      }, 2000);
    } else {
      window.scrollTo({ left: 0, top: 0, behavior: 'smooth' });
      this.errorShown = true;
      setTimeout(() => {
        this.errorShown = false;
      }, 3000);
    }
  }
}
