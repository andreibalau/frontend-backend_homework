import mongoose from "mongoose";
import User from "../models/User";
import ToDoList from "../models/ToDoList";
import ListElement from "../models/ListElement";
import { DATABASE_URL } from "../config/database";
import { UserInfo } from "os";

export default class DriverManager {
    private MONGO_STRING: string = DATABASE_URL;
    private static _instance: DriverManager;

    private constructor() { }

    public async connect() {
        mongoose.connect(this.MONGO_STRING, {
            useNewUrlParser: true,
            useFindAndModify: false,
            useCreateIndex: true
        });

        let db = mongoose.connection;
        db.once("open", () => {
            console.log("Connected to " + this.MONGO_STRING);
        });
    }

    public static get Instance() {
        if (this._instance) {
            return this._instance;
        } else {
            this._instance = new DriverManager();
            return this._instance;
        }
    }

    public async addNewUser(username: string,
        email: string,
        password: string,
        secretQ: string,
        secretA: string) {
        console.log(username, email, password, secretQ, secretA)
        let newUser = new User({
            username,
            email,
            password,
            secretQ,
            secretA
        });

        return await newUser.save();
    }

    // public async addNewList(listName: string) {
    //     let newList = new ToDoList({
    //         name: listName
    //     });

    //     return await newList.save();
    // }

    public async getListByName(listName: string) {
        return await ToDoList.findOne({ name: listName }).exec();
    }

    public async getAllToDoLists() {
        return await ToDoList.find({}).exec();
    }
    public async addNewList(
        username: string,
        listName: string
    ) {
        let newItem = new ToDoList({
            name: listName
        });

        return await User.findOneAndUpdate(
            { username: username },
            { $push: { todoList: newItem } }
        ).exec();
    }

    public async addNewElementForList(//this one ... i think it has a curse upon it ...
        username: string,
        listName: string,
        taskName: string,
        taskDescription: string
    ) {
        let newItem = new ListElement({
            title: taskName,
            body: taskDescription
        });
        // console.log(username, listName, taskName, taskDescription);
        // let target: any = await User.findOne({ "username": username },
        //     { "todoList": { $elemMatch: { "name": listName } } })
        // let target2: any = await User.findOne({ "username": username, "todoList.name": listName })
        // console.log(target);
        // console.log(target2);
        // console.log(User.findOne({ username: username, todoList: { $elemMatch: { name: listName } } }));
        console.log("wkmkwwlwx");
        console.log(await User.findOneAndUpdate(
            { "username": username, "todoList.name": listName },
            { $push: { "todoList.listElements": newItem } }//in this way i get error yeey
        ));
        return await User.findOneAndUpdate(
            { "username": username, "todoList.name": listName },
            { $push: { "todoList.listElements": newItem } }
        ).exec();
    }
}