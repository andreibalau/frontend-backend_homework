import { IToDoList, ToDoListSchema } from "./ToDoList";
import mongoose, { Schema, Document } from "mongoose";

export interface IUser extends Document {
	username: string;
	email: string;
	password: string;
	secretQ: string;
	secretA: string;
	todoList: IToDoList[];

	validPassword(password: string): boolean;
}

const UserSchema = new Schema(
	{
		username: {
			type: String,
			required: true,
			unique: true
		},
		email: {
			type: String,
			required: true,
			unique: true
		},
		password: {
			type: String,
			required: true
		},
		secretQ: {
			type: String,
			required: true,
		},
		secretA: {
			type: String,
			required: true,
		},
		todoList: {
			type: [ToDoListSchema]
		},
	},
	{ collection: "User" }
);
UserSchema.methods.validPassword = function (password: string) {
	return password === this.password;
};

export default mongoose.model<IUser>("User", UserSchema);
